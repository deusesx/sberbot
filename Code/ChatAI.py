# coding: utf-8
import os

import pandas as pd
from nltk.corpus import stopwords
from pymystem3 import Mystem

from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.cluster import KMeans
from scipy.spatial.distance import cosine

from sklearn.externals import joblib

class ChatAI(object):
    stem = Mystem(entire_input=False)
    
    @staticmethod
    def clean_text(x):
        x = ' '.join([s for s in ChatAI.stem.lemmatize(x) if s not in stopwords.words('russian')])
        return x
    
    def __init__(self, Vectorizer=None, ClusterModel=None):
        if Vectorizer is None:
            self.Vectorizer = TfidfVectorizer(min_df=5, max_df=0.9, preprocessor=ChatAI.clean_text)
        else:
            self.Vectorizer = Vectorizer
        
        if ClusterModel is None:
            self.ClusterModel = KMeans(n_clusters=140)
        else:
            self.ClusterModel = ClusterModel
    
    def train(self, QuestionAnswerDB):
        self.QuestionAnswerDB = QuestionAnswerDB
        if 'vocabulary_' not in self.Vectorizer.__dict__.keys(): 
            self.q_vectors = self.Vectorizer.fit_transform(self.QuestionAnswerDB.question)
        else:
            self.q_vectors = self.Vectorizer.transform(self.QuestionAnswerDB.question)
        if 'cluster_centers_' not in self.ClusterModel.__dict__.keys():
            self.ClusterModel.fit_transform(self.q_vectors)
        if not ('q_vect' in self.QuestionAnswerDB.columns and 'q_cluster' in self.QuestionAnswerDB.columns):    
            self.QuestionAnswerDB['q_vect'] = pd.Series(t for t in self.q_vectors.todense())
            self.QuestionAnswerDB['q_cluster'] = self.ClusterModel.labels_
        self.trained_ = True
    
    def find_answer(self, question_string, top_k=5):
        q_vect = self.Vectorizer.transform(question_string)
        cl = self.ClusterModel.predict(q_vect)[0]
        same_cluster_db = self.QuestionAnswerDB[self.QuestionAnswerDB.q_cluster == cl]
        q_vect = [x for x in q_vect.todense()][0]
        same_cluster_db['cosine'] = same_cluster_db['q_vect'].apply(lambda x: 1 - cosine(x, q_vect))
        same_cluster_db = same_cluster_db.sort_values(by='cosine', ascending=False)
        top_k = same_cluster_db[:top_k][['question', 'answer', 'cosine']]
        response = []
        for q, a, cos in zip(top_k.question, top_k.answer, top_k.cosine):
            response.append({'question' : q, 'answer': a, 'confidence': cos})
        return response
    
    def safe(self, filename):
        os.makedirs(filename)
        with open('%s/clusterer.pkl' % filename, 'wb') as f:
            joblib.dump(self.ClusterModel, f)
        with open('%s/vectorizer.pkl' % filename, 'wb') as f:
            joblib.dump(self.Vectorizer, f)
        with open('%s/database.pkl' % filename, 'wb') as f:
            joblib.dump(self.QuestionAnswerDB, f)
            
    def load(self, filename):
        with open('%s/clusterer.pkl' % filename, 'rb') as f:
            self.ClusterModel = joblib.load(f)
        with open('%s/vectorizer.pkl' % filename, 'rb') as f:
            self.Vectorizer = joblib.load(f)
        with open('%s/database.pkl' % filename, 'rb') as f:
            self.QuestionAnswerDB = joblib.load(f)